# Data analysis with R ArcGIS bridge.

## Analysising SanFrancisco Crime data using the R ArcGIS bridge. 

### Requirements
1. R 3.6.1 
2. RStudio (for flexibility) 
3. ArcGIS 10.4

Note: if ur arcgis is 32bit, then your r version should also be 32bit and vice versa. ArcGIS should be 10.3 and above. 
R version should also be 3.2.2 and above. 

### Steps
1. Download the r bridge installer (contains the R installation scripts) https://github.com/R-ArcGIS/r-bridge-install/archive/master.zip
and the Sanfrancisco crimes dataset http://downloads.esri.com/learnarcgis/analyze-crime-using-statistics-and-the-r-arcgis-bridge/san-francisco.zip. 
The San Francisco Crimes folder contains the crimes committed in SanFrancisco.
2. Start ArcGIS, open the catalog and then create a folder connection to the r bridge installer and then install the R installation scripts.
3. Create another folder connection to the SanFrancisco Crimes folder,  expand the SF_Crime geodatabase and then right click the
San_Francisco_crimes feature class, select manage and then click compress file. 
4. Aggregate our crime data before we start the analysis process using the "create space time cube by Aggregating points" tool. 
Aggregation will help us reveal the spatial and temporal relationships in the data that we may not have seen. 
5. Analyse the crime hot spots in the SanFrancisco area using the "Emerging HotSpot Analysis" tool. 


At this point, we now have our analyzed crime data in ArcGIS. Our next step is to bridge our crime data into R. 
NB. Before we proceed, we should leave our ArcGIS open and then open the r terminal or use RStudio to access the r terminal. 

```
#load the arcgisbinding package in R
library(arcgisbinding)
#asks r to print information about our ArcGIS product and License. 
arc.check_product()
```

Make sure the above two steps are working properly in order for you to proceed. 

6. load the arcgis data in the R terminal using the arc.open()  function. 
```
arc.open("C:/Users/Janx/Documents/ArcGIS/san-francisco/SF_Crime.gdb/San_Francisco_Crimes")
#store it in a variable
enrich_df <- arc.open("C:/Users/Janx/Documents/ArcGIS/san-francisco/SF_Crime.gdb/San_Francisco_Crimes")
```

7. Create the R data frame object with the number of data field attributes. 
```
enrich_select_df <- arc.select(object = enrich_df, fields = c("OBJECTID", "Shape", "Field1", "Dates", "Category", "Descript", "DayOfWeek", "PdDistrict", "Resolution", "Address", "X", "Y"))
```

8. Convert the R data frame into a spatial data frame object using the arc.data2sp() function. This will be archived by installing 
the sp package in R that contains that function. 
```
#install the sp package
install.packages("sp")
#load the sp package
library(sp)
#convert the r data frame data into a spatial data frame object.
enrich_spdf <- arc.data2sp(enrich_select_df)
#create the column names from the data
col_names <- c("OBJECTID", "Shape", "Field1", "Dates", "Category", "Descript", "DayOfWeek", "PdDistrict", "Resolution",  "Address", "X", "Y")
#update column names
col_names <- colnames(enrich_spdf@data)
```

9. Perform Empirical Bayes using the EBest() function to smooth our crime data rates.
```
#install spdep package 
install.packages("spdep")
# load the data
library(spdep)
####### Empirical Bayes
n <- enrich_spdf@data$OBJECTID
x <- enrich_spdf@data$Field1
EB <- EBest (n, x)
b <- attr(EB, "parameters")$b
a <- attr(EB, "parameters")$a
v <- a + (b/x)
v[v < 0] <- b/x
z <- (p - b)/sqrt(v)
enrich_spdf@data$EB_Rate <- z
arcgis_df <- arc.sp2data(enrich_spdf)
```

#### writing the data back to the arcgis project with a new name San_Francisco_Crime_Rates.
arc.write('C:/Users/Janx/Documents/ArcGIS/San-Francisco/SF_Crime.gdb/San_Francisco_Crime_Rates', arcgis_df, shape_info = arc.shapeinfo(enrich_df))

NB: At this point, I have used the R-ArcGIS bridge to transfer our crime data into R to calculate the smoothed crime data rates. 
I can now transfer our crime data back into ArcGIS to continue with our analysis and to further pinpoint areas in need of extra police 
resources to reduce the number of crimes occurring

### Our Next Step
Am going to explore our crime data in R to determine whether the trends in crime data rates can be linked to any of the other attributes in 
our enriched data.

1. load the San_Francisco_Crime_Rates data from arcgis
2. select the variables you want from the crimes data to bring into R
3. Convert the feature class into a spatial data frame object using the arc.data2sp() function
4. create functions for the correlation matrix
5. install and load the reshape2, ggplot2, and ggmap libraries
5. create the correlation matrix

